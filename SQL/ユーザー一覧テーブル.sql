CREATE TABLE user(
id int(11) NOT NULL AUTO_INCREMENT,
login_id varchar(256) NOT NULL,
password varchar(256) NOT NULL,
name varchar(256) NOT NULL,
birth_date date NOT NULL,
address varchar(256) NOT NULL,
create_date datetime NOT NULL);
