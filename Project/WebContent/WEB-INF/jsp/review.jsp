<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カスタマーレビュー</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="CSS/style.css" rel="stylesheet" type="text/css" />
</head>


<body>
 <div class="header">
	  <div class="log" align="left">
	    <h2>温泉予約管理システム</h2>
	  </div>
	  <div class="lists" align="right">
		<ul class="list">
		<li>${userInfo.name}さん</li>
		<li><a href="HotSpringListServlet">温泉リスト</a></li>
		<li><a href="LogoutServlet">ログアウト</a></li>
		</ul>
	  </div>
	</div>


<h2>あなたの口コミを投稿</h2>

<form action ="ReviewResultServlet" method="post">
<input type="hidden" name="hotSpringId" value="${data.id}">

<div>
	●当店をお知りになったきっかけは？<br>
	<label><input type="checkbox" name="how" value="知り合いの紹介で知った">知り合いの紹介で</label>
	<label><input type="checkbox" name="how" value="雑誌・Webサイトを見て知った">雑誌・Webサイトを見て</label>
</div>

<div>
	●当店の評価点(★最大5つ)<br>
	<input type="number" name="star" max="5" placeholder="★最大5つ">
</div>
<div>
   <label>●ご利用者レビュー<br>
	<textarea name="review"></textarea></label>
</div>
<div>
    <input type="submit" name="action" value="送信">
</div>

</form>

</body>
<a href="javascript:history.back()">[戻る]</a>
</html>