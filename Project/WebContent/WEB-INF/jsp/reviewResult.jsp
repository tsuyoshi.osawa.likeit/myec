<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カスタマーレビュー</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="CSS/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="header">
	  <div class="log" align="left">
	    <h2>温泉予約管理システム</h2>
	  </div>
	  <div class="lists" align="right">
		<ul class="list">
		<li><a href="HotSpringListServlet">温泉リスト</a></li>
		<li><a href="LogoutServlet">ログアウト</a></li>
		</ul>
	  </div>
	</div>
<div class="login">
ご記入ありがとうございました。
</div>
</body>
<a href="HotSpringListServlet" class="navbar-link login-link">一覧画面へ戻る</a>
</html>