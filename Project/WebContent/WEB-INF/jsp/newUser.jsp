<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規ユーザー</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="CSS/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div class="header">
	  <div class="log" align="left">
	    <h2>温泉予約管理システム</h2>
	  </div>
	  <div class="lists" align="right">
		<ul class="list">
		<li><a href="HotSpringListServlet">温泉リスト</a></li>
		</ul>
	  </div>
	</div>

<div class="login"align="center">

			<br>

				<h2>新規登録</h2>

			<br>
			<br>
<div class="container">
<div class="card card-body bg-light">
			<div class="row">
				<div class="col-md">
					<form class="form-signin" action="NewUserServlet" method="post">
						<div class="form-group">
							<input type="text" name="login_id" class="form-control" value=""
								placeholder="ログインIDを入力">
						</div>
                    <br>
						<div class="form-group">
							<input type="password" name= "password" class="form-control" value=""
								placeholder="パスワードを入力">
						</div>
					<br>
						<div class="form-group">
							<input type="password" name="passwordConfirm" class="form-control" value=""
								placeholder="パスワード(確認)を入力">
						</div>
					<br>
						<div class="form-group">
							<input type="text" name="name" class="form-control" value=""
								placeholder="氏名を入力">
						</div>
					<br>
						<div class="form-group">
							<input type="date" name="birth_date" class="form-control" value="">
						</div>
					<br>
						<div class="form-group">
							<input type="text" name="address"class="form-control" value=""
								placeholder="住所を入力">
						</div>
					<br>


		<button class="btn btn-primary" type="submit" name="action">登録</button>
<p><font color="red">
 <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
</c:if>
<c:if test="${errMsg1 != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg1}
		</div>
</c:if>
<c:if test="${errMsg2 != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg2}
		</div>
</c:if>
</font></p>
					</form>
				</div>

			</div>
		</div>
	</div>
</div>

</body>
</html>