<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧リスト</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="CSS/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="header">
	  <div class="log" align="left">
	    <h2>温泉予約管理システム</h2>
	  </div>
	  <div class="lists" align="right">
		<ul class="list">
		<li><a href="NewUserServlet">新規登録</a></li>
		<li><a href="HotSpringListServlet">温泉リスト</a></li>
		</ul>
	  </div>
</div>

<div class="login"align="center">
		<div class="container">

</div>
</div>
<div><div class="card card-body bg-light">
			<br>

				<h2>登録確認</h2>

			<br>
			<br>

			<div class="row">
				<div class="col-md">
					<form action="UserDetailServlet" method="post">
						<div class="form-group">
						<lavel>ID:</lavel>
						<td><c:out value="${user.loginId}" /></td>
						</div>
                    <br>
						<div class="form-group">
						<lavel>氏名:</lavel>
						<td><c:out value="${user.name}" /></td>
						</div>
					<br>
						<div class="form-group">
						<lavel>生年月日:</lavel>
						<td><c:out value="${user.birthDate}" /></td>
						</div>
					<br>
						<div class="form-group">
					    <lavel>登録日時:</lavel>
				     	<td><c:out value="${user.createDate}" /></td>
						</div>
					<br>
						<div class="form-group">
	    				<lavel>更新日時:</lavel>
	                    <td><c:out value="${user.updateDate}" /></td>
				        </div>
				        </form>
		         </div>
		     </div>


	</div>
	</div>
<div>
<br>
<a href="UserListServlet" class="navbar-link login-link">戻る</a>
</div>
</body>
</html>