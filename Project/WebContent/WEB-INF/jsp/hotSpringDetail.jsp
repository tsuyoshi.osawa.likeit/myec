<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>温泉詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="CSS/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div class="header">
		<div class="log" align="left">
			<h2>温泉予約管理システム</h2>
		</div>
		<div class="lists" align="right">
			<ul class="list">
				<li>${userInfo.name}さん</li>
				<li><a href="LogoutServlet">ログアウト</a></li>
			</ul>
		</div>
	</div>
<body>
	<div class="login" align="center">
		<h2>詳細画面</h2>
	</div>

	<form class="form-signin" action="ReviewServlet" method="post">
		<input type="hidden" name="id" value="${data.id}">
		<div class="container">
			<div class="row">

				<div class="col-md-4">
					<div class="card mb-4 shadow-sm">
						<img src="img/${data.fileName}" width="100%" height="400">
						<div class="card-body">
							<p class="card-text">${data.name}</p>
							<p class="card-text">${data.open}</p>

<c:if test="${userInfo.name != null }">
							<div class="d-flex justify-content-between align-items-center">
								<div class="btn-group">
									<a class="btn btn-primary"
										href="BuyReservationServlet?id=${data.id}">予約</a>
								</div>
								<small class="text-muted">9 mins</small>
							</div>
</c:if>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<p>${data.detail}</p>
					<p>${data.access}</p>
					<hr>

<c:if test="${userInfo.name != null }">
					<a class="btn btn-success" href="ReviewServlet?id=${data.id}">口コミを書く</a>
</c:if>
					<p>■■ご利用者様の評価■■</p>

					<c:forEach var="user" items="${reviewData}">
					<input type="hidden" name="how" value="${user.how}">
						<ul class="reviewList">
							<li>●${user.createDate}</li>
							<c:if test="${user.star == 1 }">
								<li>★☆☆☆☆</li>
							</c:if>
							<c:if test="${user.star == 2 }">
								<li>★★☆☆☆</li>
							</c:if>
							<c:if test="${user.star == 3 }">
								<li>★★★☆☆</li>
							</c:if>
							<c:if test="${user.star == 4 }">
								<li>★★★★☆</li>
							</c:if>
							<c:if test="${user.star == 5 }">
								<li>★★★★★</li>
							</c:if>
							<li>${user.review}</li>
						</ul>
					</c:forEach>

				</div>
			</div>
		</div>
	</form>


	<a href="javascript:history.back()">[戻る]</a>
</body>
</html>