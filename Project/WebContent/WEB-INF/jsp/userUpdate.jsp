<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー更新画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="CSS/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="header">
	  <div class="log" align="left">
	    <h2>温泉予約管理システム</h2>
	  </div>
	  <div class="lists" align="right">
		<ul class="list">
		<li>${userInfo.name}さん</li>
		<li><a href="NewUserServlet">新規登録</a></li>
		<li><a href="HotSpringListServlet">温泉リスト</a></li>
		</ul>
	  </div>
</div>
<div class="login"align="center">
		<div class="border col-7">
			<br>

				<h2>登録確認</h2>


			<br>
			<br>

			<div class="row">
				<div class="col-md">
				<form class="form-signin" action="UserUpdateServlet" method="post">
					<input type="hidden" name="id" value="${user.id}">
					<input type="hidden" name="loginId" value="${user.loginId}">

						<div class="form-group">
							<label>ID:
							<a><c:out value="${user.loginId}" /></a>
							</label>
						</div>
                    <br>
						<div class="form-group">
							<input type="text" name ="password" class="form-control"
								placeholder="パスワードを入力">

						</div>
					<br>
						<div class="form-group">
							<input type="text" name ="passwordConfirm" class="form-control"
								placeholder="パスワード(確認)を入力">
						</div>
					<br>
						<div class="form-group">
							<input type="text" name = "userName" class="form-control" value="${user.name}"
								placeholder="氏名を入力">
						</div>
					<br>
						<div class="form-group">
							<input type="date" name="userBirthDate" class="form-control" value="${user.birthDate}"
								placeholder="氏名を入力">
						</div>
					<br>
						<button class="btn btn-primary" type="submit" name="action">更新</button>

						<p>	<font color="red">
								<c:if test="${errMsg != null}">
								<div class="alert alert-danger" role="alert">${errMsg}</div>
								</c:if>

								<c:if test="${errMsg1 != null}">
								<div class="alert alert-danger" role="alert">${errMsg1}</div>
								</c:if>
							</font>
						</p>

		</form>
				</div>
			</div>

			</div>
		</div>
<a href="UserListServlet" class="navbar-link login-link">戻る</a>
</body>
</html>