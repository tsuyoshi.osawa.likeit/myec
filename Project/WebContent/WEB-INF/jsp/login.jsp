<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="CSS/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
 <div class="header">
	  <div class="log" align="left">
	    <h2>温泉予約管理システム</h2>
	  </div>
	  <div class="lists" align="right">
		<ul class="list">
		<li><a href="NewUserServlet">新規登録</a></li>
		<li><a href="HotSpringListServlet">温泉リスト</a></li>
		</ul>
	  </div>
</div>

  <div class="login" align="center">
    <form class= "form-signin"action="LoginServlet" method="post">


  <h1>ログイン画面</h1>
  <br>
  <br>
<div class="container">
<div class="card card-body bg-light">
			<div class="row">
				<div class="col-md">

     <div class="form-group">
       <label>ログインID:</label>
       <input type=password name= "loginId" value=""placeholder="IDを入力">
     </div>

     <div class="form-group">
       <label>パスワード:</label>
        <input type=password name= "password" value=""placeholder="パスワードを入力">
      </div>
     		 <p class="center-align">
			<button class="btn btn-primary" type="submit"name="action">ログイン</button>
			</p>

			<p><font color="red">
	    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
		</c:if></font></p>


  		<p class="mt-5 mb-3 text-muted">&copy; 2019</p>
</div></div></div></div>
   </form>

</div>

</body>
</html>

