<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧リスト</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="CSS/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!-- header -->
    <header>
    <div class="header">
      <div class="log" align="left">
		<h2>温泉予約管理システム</h2>
	 </div>
		<div class="lists" align="right">
         <ul class="list">
            <li>${userInfo.name}さん</li>
            <li><a href="HotSpringListServlet">温泉リスト</a></li>
            <li><a href="LogoutServlet">ログアウト</a></li>
  		 </ul>
  		 </div>
    </div>


    </header>


    <div class="container">

 <form class="form-signin" action="UserListServlet" method="post">
      <div class="panel-body">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">検索条件</div>
            </div>
            <br>
            <div class="panel-body">

                <div class="form-group">
                  <label for="code" class="control-label col-sm-2">ログインID</label>
                  <div class="col-sm-6">
                    <input type="text" name="login_id" id="login-id" class="form-control"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="control-label col-sm-2">ユーザ名</label>
                  <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="continent" class="control-label col-sm-2">生年月日</label>
                  <div class="row">
                    <div class="col-sm-2">
                      <input type="date" name="date-start" id="date-start" class="form-control" size="30"/>
                    </div>
                    <div class="col-xs-1 text-center">
                      ~
                    </div>
                    <div class="col-sm-2">
                      <input type="date" name="date-end" id="date-end" class="form-control"/>
                    </div>
                </div>
                </div>
                <div class="text-right">
                  <button type="submit" value="検索" class="btn btn-primary form-submit">検索</button>
                </div>

            </div>
        </div>

        <div class="table-responsive">
    <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
         <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->

             <c:choose>
                  <c:when test="${userInfo.loginId=='admin'}">
                     <td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                     </td>
				 </c:when>
				 <c:otherwise>
					  <c:if test="${userInfo.loginId==user.loginId}">
					  <td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                      </td>
                      </c:if>
				 </c:otherwise>
			</c:choose>
                   </tr>
                 </c:forEach>
         </tbody>
     </table>
           </div>
         </div>
      </div>
</form>
 </body>
</html>