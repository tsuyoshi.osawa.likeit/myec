<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>予約画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="CSS/style.css" rel="stylesheet" type="text/css" />
</head>


<body>
   <div class="header">
	  <div class="log" align="left">
	    <h2>温泉予約管理システム</h2>
	  </div>
	  <div class="lists" align="right">
		<ul class="list">
		<li>${userInfo.name}さん</li>
		<li><a href="HotSpringListServlet">温泉一覧</a></li>
		<li><a href="LogoutServlet">ログアウト</a></li>
		</ul>
	  </div>
	</div>

<div class="login"align="center">
	<h2>予約履歴</h2>
</div>
<br>
<br>
<form action="BuyReservationHistoryServlet" method="post">
<input type="hidden" name="reservationDate" value="${reservationDate}">

<div class="container">
<div class="card card-body bg-light">
   <div class="row">
	<table class="table">
	<thead>
	  <tr>
        <th scope="col">予約日時</th>
        <th scope="col">温泉名</th>
        <th scope="col">合計金額</th>
      </tr>
     </thead>

	<tbody>
  <c:forEach var="user" items="${reservationData}">


	<tr>
      <td>${user.formatDate}</td>
      <td>${user.hotSpringName}</td>
      <td>${user.hotSpringPrice}円</td>
    </tr>
  </c:forEach>
	</tbody>
	</table>
   </div>
	<br>
	<br>

<div align="center">
	<a href="HotSpringListServlet" class="navbar-link login-link">温泉一覧画面へ戻る</a>
</div>
</div>
</div>
</form>
</body>
</html>