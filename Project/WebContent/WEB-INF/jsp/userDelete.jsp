<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="CSS/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="header">
	  <div class="log" align="left">
	    <h2>温泉予約管理システム</h2>
	  </div>
	  <div class="lists" align="right">
		<ul class="list">
		<li>${userInfo.name} さん </li>
		<li><a href="NewUserServlet">新規登録</a></li>
		<li><a href="HotSpringListServlet">温泉リスト</a></li>
		</ul>
	  </div>
</div>
<div class="login"align="center">
		<div class="border col-7">
			<br>

				<h2>削除確認</h2>



<div class="row">
	<div class="col-md">
				<form class="form-signin" action="UserDeleteServlet" method="post">
					<input type="hidden" name="id" value="${user.id}">
					<input type="hidden" name="loginId" value="${user.loginId}">

						<div class="form-group">
							<label>ID:
							<a><c:out value="${user.loginId}" /></a>
							</label>
						<a>を本当に削除しても宜しいでしょうか。</a>
						</div>
						<br>
						<input class="btn btn-primary" type="submit" value="OK" style="margin-left:250px; float:left; ">

				</form>

						<a href="UserListServlet" class="navbar-link login-link">
						<input class="btn btn-primary" type="submit" value="キャンセル" style="margin-right:200px; float:right;">
						</a>



     </div>
</div>
      </div>
</div>
<div>
<br>
<a href="UserListServlet" class="navbar-link login-link">戻る</a>
</div>
</body>
</html>