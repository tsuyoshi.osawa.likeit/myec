<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>関東温泉一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="CSS/style.css" rel="stylesheet" type="text/css" />
</head>


<body>

	<div class="header">
		<div class="log" align="left">
			<h2>温泉予約管理システム</h2>
		</div>
		<div class="lists" align="right">
		<form action="BuyReservationHistoryServlet" method="post">
		　　	<ul class="list" >
<c:if test="${userInfo.name != null }">
				<li>${userInfo.name}さん</li>
</c:if>
<c:if test="${userInfo.name == null }">
			    <li><a href="NewUserServlet">新規登録</a></li>
				<li><a href="LoginServlet">ログイン</a></li>
</c:if>
<c:if test="${userInfo.name != null }">
				<li><a href="LogoutServlet" >ログアウト</a></li>
		　　　		<li><Button type="submit">${userInfo.name}さんの予約履歴</Button></li>
</c:if>
			</ul>
		</form>

		</div>
	</div>

<form class="form-signin" action="HotSpringListServlet" method="post">
	<div class="login" align="center">

		<h2>～心を満たせる！やすらぎ温泉～</h2>

			<p>気になる温泉を探そう!
			<input type="text" name="name" id="search" placeholder="検索キーワードを入力">
			 <button type="submit" value="検索" class="btn btn-primary form-submit">検索</button>
			</p></div>
<div class="container">

		<div class="row">
			<c:forEach var="d" items="${data}">
				<div class="col-md-4">
					<div class="card mb-4 shadow-sm">
						<img src="img/${d.fileName}" width="100%"
							height="225">
						<div class="card-body">
							<p class="card-text">${d.name}</p>
							<p class="card-text">${d.open}</p>




							<div class="d-flex justify-content-between align-items-center">
								<div class="btn-group">
										<a class="btn btn-primary" href="HotSpringDetailServlet?id=${d.id}">詳細</a>　　
									<c:if test="${userInfo.name != null }">
										<a class="btn btn-primary" href="BuyReservationServlet?id=${d.id}">予約</a>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>

	</div></div>
</form>

</body>
</html>