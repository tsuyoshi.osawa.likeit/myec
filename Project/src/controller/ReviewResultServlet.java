package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.HotSpringDao;
import dao.ReviewDao;
import model.HotSpringBeans;
import model.User;

/**
 * Servlet implementation class ReviewResultServlet
 */
@WebServlet("/ReviewResultServlet")
public class ReviewResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewResultServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");
		System.out.println(id);


		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		HotSpringDao hotSpringDao = new HotSpringDao();
		HotSpringBeans hotSpringBeans = hotSpringDao.detail(id);
		request.setAttribute("data", hotSpringBeans);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reviewResult.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

/////////  int型➡getParameterが無いため、String型でgetParameterを用意して取ってくる

		 String star = request.getParameter("star");
    	 String review = request.getParameter("review");
    	 String hotSpringId = request.getParameter("hotSpringId");
    	 int str =Integer.parseInt(star);
    	 String how = request.getParameter("how");

    	 request.setAttribute("review",review);
 		 request.setAttribute("star",str);

 		 User user =(User)session.getAttribute("userInfo");

 		ReviewDao  reviewDao= new ReviewDao();
 		ReviewDao.insertReview(user.getId(),review, hotSpringId, str,how);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reviewResult.jsp");
		dispatcher.forward(request, response);
	}

}
