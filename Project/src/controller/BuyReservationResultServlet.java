package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyReservationDao;
import model.User;
/**
 * Servlet implementation class ReservationResultServlet
 */
@WebServlet("/BuyReservationResultServlet")
public class BuyReservationResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyReservationResultServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reservationResult.jsp");
	dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();






    	 String reservationDate = request.getParameter("reservationDate");
    	 String hotSpringName = request.getParameter("hotSpringName");
    	 String hotSpringPrice = request.getParameter("hotSpringPrice");


    	    request.setAttribute("reservationDate",reservationDate);
    		request.setAttribute("hotSpringName",hotSpringName);
    		request.setAttribute("hotSpringPrice",hotSpringPrice);

    	User user =(User)session.getAttribute("userInfo");
    	String hotSpringId = request.getParameter("hotSpringId");



			BuyReservationDao buyReservationDao = new BuyReservationDao();
			BuyReservationDao.insertBuy(user.getId(), hotSpringId, reservationDate);



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reservationResult.jsp");
		dispatcher.forward(request, response);
		}
}
