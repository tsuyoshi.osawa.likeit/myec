package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;
/**
 * Servlet implementation class NewUserServlet
 */
@WebServlet("/NewUserServlet")
public class NewUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//newUserをフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birth_date");
		String address = request.getParameter("address");

		if (password.equals(passwordConfirm)) {
		} else {
			request.setAttribute("errMsg", "入力された内容は正しくはありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(loginId == null || "".equals(loginId)){
			request.setAttribute("errMsg1", "未入力です。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return;}
		if(password == null || "".equals(password)) {
			request.setAttribute("errMsg1", "未入力です。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return;}
		if(passwordConfirm == null || "".equals(passwordConfirm)) {
			request.setAttribute("errMsg1", "未入力です。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return;}
		if(name == null || "".equals(name)) {
			request.setAttribute("errMsg1", "未入力です。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return;}
		if(birthDate == null || "".equals(birthDate)) {
			request.setAttribute("errMsg1", "未入力です。");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
		dispatcher.forward(request, response);
		return;
		}


		//userDao.againId(loginId)にあるUserに取り込む
		UserDao userDao = new UserDao();
		User user = userDao.againId(loginId);

		//ログインIDが登録済みの時
		//userに登録したloginIdがnulll(無い場合)
		if(user != null) {
		request.setAttribute("errMsg2", "ログインIDは既に登録済みです");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
		dispatcher.forward(request, response);
		return;
		}

		userDao.create(loginId, password, name, birthDate, address);

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("LoginServlet");
	}
}