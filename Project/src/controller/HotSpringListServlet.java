package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.HotSpringDao;
import model.HotSpringBeans;

/**
 * Servlet implementation class HotSpringListServlet
 */
@WebServlet("/HotSpringListServlet")
public class HotSpringListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HotSpringListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる

		// 温泉一覧情報を取得
		HotSpringDao hotSpringDao = new HotSpringDao();
		List<HotSpringBeans> hotSpringList = hotSpringDao.findAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("data", hotSpringList);

		// 温泉一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/hotSpringList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
    	// TODO 未実装：検索機能


		String name = request.getParameter("name");


		HotSpringDao hotSpringDao = new HotSpringDao();
		List<HotSpringBeans> hotSpringList = hotSpringDao.hotSpringSearch(name);
				request.setAttribute("data", hotSpringList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/hotSpringList.jsp");
		dispatcher.forward(request, response);

    }
}
