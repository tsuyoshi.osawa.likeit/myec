package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyReservationDao;
import model.ReservationDataBeans;
import model.User;

/**
 * Servlet implementation class BuyReservationHistoryServlet
 */
@WebServlet("/BuyReservationHistoryServlet")
public class BuyReservationHistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyReservationHistoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();



		User user =(User)session.getAttribute("userInfo");

		String reservationDate = request.getParameter("reservationDate");
		request.setAttribute("reservationDate",reservationDate);



		// ユーザ履歴情報を取得
		BuyReservationDao buyReservationDao = new BuyReservationDao();
		ArrayList<ReservationDataBeans> reservationList = BuyReservationDao.reservationbyUserId(user.getId());
		request.setAttribute("reservationData", reservationList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reservationHistory.jsp");
		dispatcher.forward(request, response);
}


}