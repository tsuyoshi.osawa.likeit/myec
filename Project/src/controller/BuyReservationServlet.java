package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.HotSpringDao;
import model.HotSpringBeans;

/**
 * Servlet implementation class BuyReservation
 */
@WebServlet("/BuyReservationServlet")
public class BuyReservationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyReservationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");
		System.out.println(id);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		HotSpringDao hotSpringDao = new HotSpringDao();
		HotSpringBeans confirm = hotSpringDao.buy(id);
		request.setAttribute("buy", confirm);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reservation.jsp");
		dispatcher.forward(request, response);
	}
}