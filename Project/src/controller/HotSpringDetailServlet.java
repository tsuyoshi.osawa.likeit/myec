package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.HotSpringDao;
import dao.ReviewDao;
import model.HotSpringBeans;
import model.ReviewBeans;

/**
 * Servlet implementation class HotSpringDetailServlet
 */
@WebServlet("/HotSpringDetailServlet")
public class HotSpringDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HotSpringDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");



		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");
		System.out.println(id);


		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		HotSpringDao hotSpringDao = new HotSpringDao();
		HotSpringBeans hotSpringBeans = hotSpringDao.detail(id);

		request.setAttribute("data", hotSpringBeans);





		// 口コミ情報を取得
		ReviewDao reviewDao = new ReviewDao();
		ArrayList<ReviewBeans> reviewList = reviewDao.reviewUserId(hotSpringBeans.getId());
		request.setAttribute("reviewData", reviewList);



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/hotSpringDetail.jsp");
		dispatcher.forward(request, response);
	}
}