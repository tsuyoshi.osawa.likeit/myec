package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.HotSpringDao;
import model.HotSpringBeans;

/**
 * Servlet implementation class BuyReservation
 */
@WebServlet("/BuyReservationConfirmServlet")
public class BuyReservationConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyReservationConfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
    /**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.setCharacterEncoding("UTF-8");


    	// URLからGETパラメータとしてIDを受け取る
    	String id = request.getParameter("id");
    	System.out.println(id);

    	// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
    	HotSpringDao hotSpringDao = new HotSpringDao();
    	HotSpringBeans confirm = hotSpringDao.buy(id);
    	request.setAttribute("buy", confirm);


    String reservationDate = request.getParameter("reservationDate");
    String hotSpringName = request.getParameter("hotSpringName");
	String hotSpringPrice = request.getParameter("hotSpringPrice");

	Date date = null;
	SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
	try {
		date = fmt.parse(reservationDate);
	} catch (ParseException e) {
		e.printStackTrace();
	}

	SimpleDateFormat format2 = new SimpleDateFormat("yyyy年MM月dd日 E曜日 H時mm分");
	String fmtdate = format2.format(date);





	request.setAttribute("reservationDate",fmtdate);
	request.setAttribute("hotSpringName",hotSpringName);
	request.setAttribute("hotSpringPrice",hotSpringPrice);




	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reservationConfirm.jsp");
	dispatcher.forward(request, response);

          }
}