package model;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private int id;
	private String loginId;
	private String password;
	private String name;
	private Date birthDate;
	private String address;
	private String createDate;
	private String updateDate;

public User(){

}

	public User(String loginId) {
		this.loginId = loginId;
	}



	// ログインセッションを保存するためのコンストラクタ
	public User(int id, String loginId, String name) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
	}


public User(int id, String loginId, String password, String name, Date birthDate, String address,
			String createDate, String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.password = password;
		this.name = name;
		this.birthDate = birthDate;
		this.address = address;
		this.createDate = createDate;
		this.updateDate = updateDate;
}
public User(int id, String loginId, String password, String name, Date birthDate,
		String createDate, String updateDate) {
	this.id = id;
	this.loginId = loginId;
	this.password = password;
	this.name = name;
	this.birthDate = birthDate;
	this.createDate = createDate;
	this.updateDate = updateDate;
}


public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getLoginId() {
	return loginId;
}
public void setLoginId(String loginId) {
	this.loginId = loginId;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Date getBirthDate() {
	return birthDate;
}
public void setBirth_date(Date birthDate) {
	this.birthDate = birthDate;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getCreateDate() {
	return createDate;
}
public void setCreate_date(String createDate) {
	this.createDate = createDate;
	}
public String getUpdateDate() {
	return updateDate;
}
public void setUpdateDate(String updateDate) {
	this.updateDate = updateDate;
}

}