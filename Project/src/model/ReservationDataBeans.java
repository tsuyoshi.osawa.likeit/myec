package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReservationDataBeans implements Serializable {
	private int id;
	private int userId;
	private int hotSpringId;
	private Date reservationDate;
	private String hotSpringName;
	private int hotSpringPrice;

public ReservationDataBeans() {

}


public ReservationDataBeans(int userId, int hotSpringId, Date reservationDate) {
	    this.userId = userId;
		this.hotSpringId = hotSpringId;
		this.reservationDate = reservationDate;
	}


	public ReservationDataBeans(int id, int userId, int hotSpringId, Date reservationDate) {
		this.id = id;
		this.userId = userId;
		this.hotSpringId = hotSpringId;
		this.reservationDate = reservationDate;
}

	public ReservationDataBeans(int userId, String hotSpringName, int hotSpringPrice, Date reservationDate) {
		this.userId = userId;
		this.hotSpringName = hotSpringName;
		this.hotSpringPrice = hotSpringPrice;
		this.reservationDate = reservationDate;


	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getHotSpringId() {
		return hotSpringId;
	}
	public void setHotSpringId(int hotSpringId) {
		this.hotSpringId = hotSpringId;
	}
	public Date getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}

	public String getHotSpringName() {
		return hotSpringName;
	}
	public void setHotSpringName(String hotSpringName) {
		this.hotSpringName = hotSpringName;
	}
	public int getHotSpringPrice() {
		return hotSpringPrice;
	}
	public void setHotSpringPrice(int hotSpringPrice) {
		this.hotSpringPrice = hotSpringPrice;
	}


	//ECの「BuyDataBeans」を参考！！！
	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 E曜日 H時mm分");
		return sdf.format(reservationDate);
	}
}