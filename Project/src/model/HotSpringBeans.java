package model;

import java.io.Serializable;

public class HotSpringBeans implements Serializable {
	private int id;
	private String name;
	private String open;
	private String detail;
	private int price;
	private String fileName;
	private String access;

	public HotSpringBeans() {
	}

	public HotSpringBeans(int id, String name, String open,  String detail, int price, String fileName, String access) {
		this.id = id;
		this.name = name;
		this.open = open;
		this.detail = detail;
		this.price = price;
		this.fileName = fileName;
		this.access = access;
	}
	public HotSpringBeans(int id, String name, int price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getOpen() {
		return open;
	}


	public void setOpen(String open) {
		this.open = open;
	}


	public String getDetail() {
		return detail;
	}


	public void setDetail(String detail) {
		this.detail = detail;
	}

	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getAccess() {
		return access;
	}


	public void setAccsess(String access) {
		this.access = access;
	}

}
