package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.ReviewBeans;

public class ReviewDao {

public static void insertReview(int userId, String review, String hotSpringId, int star, String how) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql ="INSERT INTO review(user_id,review,hot_spring_id,star,how,create_date) VALUES(?,?,?,?,?,now())";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			pStmt.setString(2, review);
			pStmt.setString(3, hotSpringId);
			pStmt.setInt(4, star);
			pStmt.setString(5, how);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
    }
public  ArrayList <ReviewBeans> reviewUserId(int hotSpringId) {
	Connection conn = null;
	ArrayList<ReviewBeans> reviewList = new ArrayList<ReviewBeans>();

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		String sql ="SELECT	review,star,create_date"
				+ " FROM review"
				+ "	INNER JOIN hot_spring"
				+ " ON review.hot_spring_id=hot_spring.id"
				+ "	WHERE hot_spring_id=?";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setInt(1, hotSpringId);
		ResultSet rs = pStmt.executeQuery();


		while(rs.next()) {
		String reviewData = rs.getString("review");
		int starData = rs.getInt("star");
		String createDateData = rs.getString("create_date");
		ReviewBeans review = new ReviewBeans(reviewData, starData, createDateData);
		reviewList.add(review);
		}
	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return reviewList;
}
}
