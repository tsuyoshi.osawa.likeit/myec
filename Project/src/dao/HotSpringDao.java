package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.HotSpringBeans;

public class HotSpringDao {

	/**
	 * 全ての温泉情報を取得する
	 * @return
	 */
	public List<HotSpringBeans> findAll() {
		Connection conn = null;
		List<HotSpringBeans> hotSpringList = new ArrayList<HotSpringBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM hot_spring";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);


			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String open = rs.getString("open");
				String detail = rs.getString("detail");
				int price = rs.getInt("price");
				String fileName = rs.getString("file_name");
				String access = rs.getString("access");
				HotSpringBeans data = new HotSpringBeans(id, name, open, detail, price, fileName, access);

				hotSpringList.add(data);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return hotSpringList;
	}
	public HotSpringBeans detail(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM hot_spring WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}
			int idD = rs.getInt("id");
			String nameD = rs.getString("name");
			String openD = rs.getString("open");
			String detailD = rs.getString("detail");
			int priceD = rs.getInt("price");
			String fileNameD = rs.getString("file_name");
			String accessD = rs.getString("access");
			return new HotSpringBeans(idD, nameD, openD, detailD, priceD, fileNameD, accessD);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public HotSpringBeans buy(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM hot_spring WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}
			int idD = rs.getInt("id");
			String nameD = rs.getString("name");
			int priceD = rs.getInt("price");
			return new HotSpringBeans(idD, nameD, priceD);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	//検索機能入力表示
	public List<HotSpringBeans> hotSpringSearch(String name) {
		Connection conn = null;
		List<HotSpringBeans> hotSpringList = new ArrayList<HotSpringBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			String sql = "SELECT * FROM hot_spring WHERE name LIKE ?";


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,"%" + name + "%");
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int idD = rs.getInt("id");
				String nameD = rs.getString("name");
				String openD = rs.getString("open");
				String detailD = rs.getString("detail");
				int priceD = rs.getInt("price");
				String fileNameD = rs.getString("file_name");
				String accessD = rs.getString("access");
				HotSpringBeans data = new HotSpringBeans(idD, nameD, openD, detailD, priceD, fileNameD, accessD);

				hotSpringList.add(data);
					}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
							return null;
						}
					}
				}
				return hotSpringList;
			}

}
