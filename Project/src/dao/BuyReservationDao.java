package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import model.ReservationDataBeans;

public class BuyReservationDao {


public static void insertBuy(int userId, String hotSpringId, String reservationDate) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql ="INSERT INTO reservation(user_id,hot_spring_id,reservation_date) VALUES(?,?,?)";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			pStmt.setString(2, hotSpringId);
			pStmt.setString(3, reservationDate);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
}
public static ArrayList <ReservationDataBeans> reservationbyUserId(int userId) {
	Connection conn = null;
	ArrayList<ReservationDataBeans> reservationList = new ArrayList<ReservationDataBeans>();

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		String sql ="SELECT user_id,hot_spring.name,hot_spring.price,reservation_date"
				+ " FROM reservation"
				+ " INNER JOIN hot_spring"
				+ " ON reservation.hot_spring_id=hot_spring.id"
				+ " WHERE user_id =?";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setInt(1, userId);
		ResultSet rs = pStmt.executeQuery();

		//"user_id"等、データベースと同じ形にする！！！！！！！
		while(rs.next()) {
		int userIdData = rs.getInt("user_Id");
		String hotSpringName = rs.getString("hot_spring.name");
		int hotSpringPrice = rs.getInt("hot_spring.price");
		Date reservationDate = rs.getTimestamp("reservation_date");
		ReservationDataBeans user = new ReservationDataBeans(userIdData, hotSpringName, hotSpringPrice, reservationDate);
		reservationList.add(user);
		}
	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return reservationList;
	}
}